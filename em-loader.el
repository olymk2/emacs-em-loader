;;; em-loader.el --- Load config split in org files -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Oliver Marks

;; Author: Oliver Marks <oly@digitaloctave.com>
;; URL: https://gitlab.com/olymk2/emacs-em-loader
;; Keywords: Processes tools
;; Version: 0.1
;; Created 14 June 2018
;; Package-Requires: ((emacs "24.4"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implid warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This provides a few utility functions to help manage your own custom Emacs configs,
;; It will read org files in a folder calledd bundles by default, and use .el files
;; if they exist and your org file has not changed.


;;; Code:

(setq em-loader-file-shas (list))

(defcustom em-loader-init-file user-init-file
  "Change startup path."
  :type '(string)
  :group 'em)

(defcustom em-loader-user-emacs-directory (file-name-directory user-init-file)
  "Change Emacs user folder."
  :type '(string)
  :group 'em)

(defcustom em-loader-message-buffer "* EM *"
  "Change buffer name."
  :type '(string)
  :group 'em)

(defcustom em-loader-folder "bundles"
  "Change settings folder."
  :type '(string)
  :group 'em)

(defun em-loader-buffer ()
  "Create or get our custom info buffer."
  (get-buffer-create em-loader-message-buffer))

(defun em-loader-info (msg)
  "Display an info message of MSG."
  (interactive "sInfo message to output:")
  (with-current-buffer (em-loader-buffer)
    (insert (format "INFO - %s\n" msg))))

(defun em-loader-warn (msg)
  "Display a warning message of MSG."
  (interactive "sWarn message to output:")
  (with-current-buffer (em-loader-buffer)
    (insert (format "WARN - %s\n" msg))))

(defun em-loader-erro (msg)
  "Display an error message of MSG."
  (interactive "sError message to output:")
  (with-current-buffer (em-loader-buffer)
    (insert (format "ERR  - %s\n" msg))))

(defun em-loader-verify-binary (binary)
  "Verify BINARY exists on system, to help new users setup."
  (interactive "sBinary to check for:")
  (let ((bin-path (executable-find binary)))
    (if bin-path
        (em-loader-info (format "Found binary for %s located %s" binary bin-path))
      (em-loader-warn (format "Missing %s not in system path" binary)))))

(defun em-loader-check-python-module-exists (module)
  "Check if MODULE exists and return error code."
  (interactive)
  (apply 'call-process "python3" nil
         (current-buffer) nil (list "-c" (concat "import " module))))

(defun em-loader-verify-python-module (module)
  "Check if MODULE exists and create warning if its missing."
  (interactive)
  (if (not
       (= 0 (em-loader-check-python-module-exists module)))
      (em-loader-info (format "Python module %s present" module))
      (em-loader-warn (format "Python module %s not present" module))))

(defun em-loader-settings-changed (filename sha)
  "Given a FILENAME and SHA check if the given filename has changed."
  (interactive)
  (let ((chash
	 (replace-regexp-in-string "\n$" ""
				   (shell-command-to-string
          (concat "sha256sum " filename " | cut -d \" \" -f 1")))))
    (string-equal sha chash)))

(defun em-create-directory-if-not-exists (new-directory-name)
  "Check if director exists, create NEW-DIRECTORY-NAME if it does not."
  (if (not (file-directory-p new-directory-name))
      (if (y-or-n-p (concat "Create Directory" new-directory-name))
          (progn
            (make-directory new-directory-name)))))

(defun em-loader-load-if-exists (filename)
  "Attempt to load elisp FILENAME if it exists."
  (if (file-exists-p filename)
      (progn (load-file filename)
             (em-loader-info (format "Loaded file %s" filename)))
    (em-loader-warn
     (format "Loading file %s failed, does not exist." filename))))

(defun em-loader-read-shas ()
  "Loop over text returning key/values split by =."
  (interactive)
  (let ((values (list)))
    (mapc
     (lambda (line)
       (when line
	 (let ((sp (split-string line "=")))
	   (if (= (length sp) 2)
	       (add-to-list 'values `(,(pop sp) . ,(pop sp)))))))
     (split-string
      (buffer-substring-no-properties (point-min) (point-max)) "\n"))
    values))

(defun em-loader-read-sha-map ()
  "Fetch shas from files."
  (if (file-exists-p (concat em-loader-folder ".sha"))
      (setq em-loader-file-shas
	    (with-temp-buffer
              (insert-file-contents (concat em-loader-folder ".sha"))
              (em-loader-read-shas)))))

(defun em-loader-store-file-hash (filename)
  "Generate and store hash for FILENAME."
  (interactive "sFilename:")
  (write-region
   (concat
    (file-name-nondirectory filename) "="
    (shell-command-to-string
     (concat "sha256sum " filename " | cut -d \" \" -f 1")))
   nil (concat em-loader-folder ".sha") 'append))

(defun em-loader-fetch-configs ()
  "Loop over files in config folder and attempt to load them."
  (interactive)
  (directory-files
   (concat em-loader-user-emacs-directory em-loader-folder "/") t "^\d+_\w*.org$"))

(defun em-loader-build-sha-map ()
  "Build up sha file with latest shas."
  (interactive)
  (write-region "" nil (concat em-loader-folder ".sha"))
  (mapc 'em-loader-store-file-hash (em-loader-fetch-configs)))

(defun em-loader-load-file (filename)
  "Load org FILENAME if its sha does not match previous one, else load elisp version."
  (let
      ((fname (file-name-sans-extension (file-name-nondirectory filename))))
    (let
        ((fpath-org (concat em-loader-user-emacs-directory em-loader-folder "/" fname ".org"))
         (fpath-el (concat em-loader-user-emacs-directory em-loader-folder "/" fname ".el")))
      (if (and
           (file-exists-p fpath-el)
	   (em-loader-settings-changed
            fpath-org
	    (cdr (assoc (concat fname ".org") em-loader-file-shas))))
          (progn
	    (em-loader-info (concat "Loading elisp config file " fpath-el))
	    (message "Org settings not changed loading settings from %sc" fpath-el)
	    (load-file (concat fpath-el "c")))
        (progn
	  (em-loader-info (concat "Loading org config file " fpath-org))
	  (message "%S" fpath-el)
	  (message "Org settings changed loading from %s" fpath-org)
	  (condition-case ex (org-babel-load-file fpath-org)
	    (message "Failed to load org file, possibly no elisp blocks yet ? %s %s" fpath-org ex))
	  (if (file-exists-p fpath-el)
	      (byte-compile-file (concat fpath-el))))))))

;;;###autoload
(defun em-loader-load-configs ()
  "Load all settings, save updated shas afterwards."
  (interactive)
  (setq em-loader-user-emacs-directory
	(file-name-directory em-loader-init-file))
  (em-loader-read-sha-map)
  (mapc 'em-loader-load-file (em-loader-fetch-configs))
  (em-loader-build-sha-map))

;;; (Features)
(provide 'em-loader)
;;; em-loader.el ends here
